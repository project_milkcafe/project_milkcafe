using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	private Script_SpriteStudio_PartsRoot spriteStudioRoot;
	bool set_motion;
	// Use this for initialization
	void Start () 
	{
		//ルートパーツの取得
		spriteStudioRoot = GetComponentInChildren<Script_SpriteStudio_PartsRoot>();
		set_motion = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(null == spriteStudioRoot)
		{
			int Count = transform.childCount;
			Transform InstanceTransformChild = null;
			for(int i=0; i<Count; i++)
			{
				InstanceTransformChild = transform.GetChild(i);
				spriteStudioRoot = InstanceTransformChild.gameObject.GetComponent<Script_SpriteStudio_PartsRoot>();
				if(null != spriteStudioRoot)
				{
					break;
				}
			}
		}
		if ( set_motion == false )
		{
			spriteStudioRoot.AnimationPlay((int)1, 1, 0, 1.0f);	
			set_motion = true;
		}
		transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);

	}
}
