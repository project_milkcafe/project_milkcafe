﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameSceneLoader : MonoBehaviour
{
    private AsyncOperation asyncOperation = null;
    private MapLoader mapLoader = null;
    
    void Start()
    {
        // 非同期にシーンを読み込み始める
        asyncOperation = SceneManager.LoadSceneAsync("GamePlayScene");
        asyncOperation.allowSceneActivation = false;

        mapLoader = new MapLoader();
        mapLoader.load("MapData/Stage" + SceneBridge.StageValue);
    }
    
    void Update()
    {
        // 9割読み込んだら次のシーンに移行
        if (!asyncOperation.allowSceneActivation && asyncOperation.progress >= 0.9f)
        {
            asyncOperation.allowSceneActivation = true;
            SceneBridge.mapData = mapLoader.getMapData();
            SceneBridge.mapHeight = SceneBridge.mapData.GetLength(0);
            SceneBridge.mapWidth = SceneBridge.mapData.GetLength(1);
        }
    }
}
