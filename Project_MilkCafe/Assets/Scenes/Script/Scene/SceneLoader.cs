﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
	public void gotoHowToPlay() {
		SceneManager.LoadScene("HowToPlay");
	}

	public void gotoMenu() {
		SceneManager.LoadScene("MenuScene");
	}

	public void gotoCredit(){
		SceneManager.LoadScene("CreditScene");
	}
	public void gotoConfig(){
		SceneManager.LoadScene("ConfigScene");
	}
	public void gotoStampcard(){
		SceneManager.LoadScene("StampCard");
	}
	public void gotoStoryScene(){
		SceneManager.LoadScene("StoryScene");
	}
}