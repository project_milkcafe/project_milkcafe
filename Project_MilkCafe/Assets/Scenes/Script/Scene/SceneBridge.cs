﻿using UnityEngine;
using System.Collections;

public class SceneBridge {
    static public int StageValue;   // ステージ番号
    static public int[,] mapData; // マップデータ保持用
    static public int mapWidth;   // マップの横幅
    static public int mapHeight;  // マップの縦幅
}
