﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StageSelectScene : MonoBehaviour
{
    public int StageValue;

    public void SceneLoad()
    {
        SceneBridge.StageValue = StageValue;
    }
}