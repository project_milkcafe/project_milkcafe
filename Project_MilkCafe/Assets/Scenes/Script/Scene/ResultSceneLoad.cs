﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResultSceneLoad : MonoBehaviour {
	public Text text = null;
	public GameObject[] rankPrefabs = new GameObject[8];
	public GameObject canvas = null;
	// Use this for initialization

	void Start () {
		// 取得率を計算する
		int scoreRate = (int)((float)(GameMaster.instance.scoreBoard.getTotalInCarrier()) / GameMaster.instance.scoreBoard.getTotalInWorld() * 100);
		//int scoreRate = 50;
		text.text = "Rate:" + scoreRate + "％";

		// ランクに応じた画像を取ってくる
		GameObject currentRank = null;
		if (scoreRate >= 90){
			currentRank = rankPrefabs[0];
			Debug.Log("AAA");
		}
		else if(scoreRate >= 80) {
			currentRank = rankPrefabs[1];
			Debug.Log("AA");
		}
		else if (scoreRate >= 70){
			currentRank = rankPrefabs[2];
			Debug.Log("A");
		}
		else if (scoreRate >= 60){
			currentRank = rankPrefabs[3];
			Debug.Log("B");
		}
		else if (scoreRate >= 50) {
			currentRank = rankPrefabs[4];
			Debug.Log("C");
		}
		else if (scoreRate >= 40){
			currentRank = rankPrefabs[5];
			Debug.Log("D");
		}
		else if (scoreRate >= 30){
			currentRank = rankPrefabs[6];
			Debug.Log("E");
		}
		else { 
			currentRank = rankPrefabs[7];
			Debug.Log("F");
		}

		// 選択した画像を表示
		GameObject rankImage = (GameObject)Instantiate(currentRank, new Vector3(35.0f, -325.0f), Quaternion.identity);
		//　canvas が親で　rankImage が子
		rankImage.transform.SetParent(canvas.transform, false);

		/* デバッグ用表示
		Debug.Log ("strawberrycount " + GameMaster.instance.scoreBoard.itemInCarrier[1] + "/" + GameMaster.instance.scoreBoard.itemTotalCount[1]);
		Debug.Log ("grapecount      " + GameMaster.instance.scoreBoard.itemInCarrier[2] + "/" + GameMaster.instance.scoreBoard.itemTotalCount[2]);

		Debug.Log ("\nAchievement rate:" + (int)((float)(GameMaster.instance.scoreBoard.getTotalInCarrier()) / (float)(GameMaster.instance.scoreBoard.getTotalInWorld()) * 100) + "％");
		*/
	}
	
	// Update is called once per frame
	void Update () {

	}
	public void SceneLoad () {
		Application.LoadLevel ("MenuScene");
	}
}
