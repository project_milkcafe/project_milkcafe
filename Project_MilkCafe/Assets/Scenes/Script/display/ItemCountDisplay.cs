﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemCountDisplay : MonoBehaviour {
	public int kind;
	private Image icon;
	private Text countText;

	private Color noItemColor = new Color(1.0f, 1.0f, 1.0f, 0.25f);
	private Color itemColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

	// Use this for initialization
	void Start () {
		icon = transform.FindChild("icon").gameObject.GetComponent<Image>();
		countText = transform.FindChild("count").gameObject.GetComponent<Text>();

		Sprite sprite = GameMaster.instance.itemIconTable.icons[kind];
		if (sprite == null) {
			icon.enabled = false;
			countText.enabled = false;
		}
		else {
			icon.sprite = sprite;
		}
	}
	
	// Update is called once per frame
	void Update () {
		int count = GameMaster.instance.scoreBoard.itemCount[kind];
		if(count > 99) {
			count = 99;
		}
		countText.text = "x" + count;

		if (count == 0) {
			icon.color = noItemColor;
			countText.color = noItemColor;
		}
		else {
			icon.color = itemColor;
			countText.color = itemColor;
		}
	}
}
