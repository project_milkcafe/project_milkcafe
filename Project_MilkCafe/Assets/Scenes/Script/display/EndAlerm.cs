﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndAlerm : MonoBehaviour {
	public bool isAnimationStarted = false;
	static public int save;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GameMaster.instance.gameTimer.isEnded && !isAnimationStarted){
			if (GameMaster.instance.scoreBoard.isCompleted()) {
				GetComponent<Animator>().SetBool("isItemCompleted", true);
			}
			else {
				GetComponent<Animator>().SetBool("isTimeUp", true);
			}
			isAnimationStarted = true;
		}
	}

	public void OnAnimationEnded() {
		GetComponent<Animator>().enabled = false;
		SceneManager.LoadScene("ResultScene");

	}
}
