﻿using UnityEngine;
using System.Collections;

public class NeedleRotater : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		transform.eulerAngles = new Vector3(0, 0, 90);
	}
	
	// Update is called once per frame
	void Update () {
		
		//transform.Rotate (Vector3.forward, -1);
		transform.rotation *= Quaternion.Euler (new Vector3 (0, 0, -1));
		float timeLimit = GameMaster.instance.gameTimer.timeLimit;
		float currentTime = GameMaster.instance.gameTimer.currentTime;
		if (currentTime < 0.0f) currentTime = 0.0f;
		transform.eulerAngles = new Vector3(0, 0, currentTime / timeLimit * 360 - (-90));
	}
}

