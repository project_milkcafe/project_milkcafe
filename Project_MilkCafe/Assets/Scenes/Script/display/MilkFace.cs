﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MilkFace : MonoBehaviour {
	public float[] distances = new float[] {60, 120, 180};
	public Sprite[] sprites;
	public AudioClip[] se;

	public Image image;
	public AudioSource audioSource;
	private int prevState;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		float dist = GameMaster.instance.positionCollection.nearestItemDistance;
		int i;
		for (i = 0; i < distances.Length; i++) {
			if (dist <= distances[i]) {
				break;
			}
		}
		if(i != prevState){
			image.sprite = sprites[i];
			audioSource.PlayOneShot(se[i]);
			prevState = i;
		}

	}
}