﻿using UnityEngine;
using System.Collections;

public class StartAlarm : MonoBehaviour {
	/**アニメーションが終わったときに呼び出される関数*/
	public void OnAnimationEnded () {
		GameMaster.instance.gameTimer.startGameTimer();
		Destroy(this.gameObject);
	}
}
