﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {
	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		anim.enabled = GameMaster.instance.gameTimer.isStarted;
	}
}

