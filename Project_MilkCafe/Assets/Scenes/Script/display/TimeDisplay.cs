﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeDisplay : MonoBehaviour {
	public Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		int t = Mathf.CeilToInt(GameMaster.instance.gameTimer.currentTime);
		if(t < 0){
			t = 0;
		}
		int minutes = t / 60;
		int seconds = t % 60;
		string strSeconds = "0" + seconds;
		text.text = minutes + ":" + strSeconds.Substring(strSeconds.Length - 2);
	}

}
