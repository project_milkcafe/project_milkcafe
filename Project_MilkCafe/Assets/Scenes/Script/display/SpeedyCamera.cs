﻿using UnityEngine;
using System.Collections;

public class SpeedyCamera : MonoBehaviour {
	public Camera camera = null;
	public Rigidbody target;
	public float defaultFOV = 60.0f;
	public float maxFOV = 68.0f;
	public float changeRate = 0.04f;
	public float sensibility;
	public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float velocity = target.velocity.magnitude;
		if (velocity >= sensibility) {
			float rate = velocity / (speed + sensibility);
			if (rate > 1.0f) {
				rate = 1.0f;
			}
			camera.fieldOfView = camera.fieldOfView * (1.0f - changeRate) + (defaultFOV * (1.0f - rate) + maxFOV * rate) * changeRate;
		}
		else {
			camera.fieldOfView = camera.fieldOfView * (1.0f - changeRate) + defaultFOV * changeRate;
		}
	}
}
