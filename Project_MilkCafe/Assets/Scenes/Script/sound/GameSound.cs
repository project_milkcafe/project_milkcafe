﻿using UnityEngine;
using System.Collections;

public class GameSound : MonoBehaviour {
	public AudioSource audioSource = null;
	public AudioClip digSound;

	public void playDigSound(){
		//SEを鳴らす。
		audioSource.PlayOneShot(digSound);
	}

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>(); 
	}

	// Update is called once per frame
	void Update () {
	
	}
}