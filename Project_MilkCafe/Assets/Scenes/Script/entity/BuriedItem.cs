﻿using UnityEngine;
using System.Collections;

public class BuriedItem : MonoBehaviour {
	public int kind;
	public bool isAnimation = false;
	public float yBase = 0.0f;
	void Start(){
		GameMaster.instance.positionCollection.items.Add(gameObject);
	}

	public void appear(){
		GameMaster.instance.scoreBoard.itemCount[this.kind]++;
		GameMaster.instance.positionCollection.items.Remove(gameObject);
		this.GetComponent<SpriteRenderer>().enabled = true;
		transform.position = GameMaster.instance.positionCollection.player.transform.position + Vector3.up * 10.0f;
		yBase = transform.position.y; 
		isAnimation = true;
	}

	void Update(){
		if(isAnimation == true){
			transform.Translate(Vector3.up * 0.5f);
			if(transform.position.y - yBase >= 30.0f){
				Destroy(this.gameObject);
			}
		}
	}
}
