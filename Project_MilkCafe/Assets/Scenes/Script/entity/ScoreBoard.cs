﻿using UnityEngine;
using System.Collections;

public class ScoreBoard : MonoBehaviour {
	public int[] itemCount = new int[6];
	public int[] itemTotalCount = new int[6];
	public int[] itemInCarrier = new int[6];

	public bool hasItem() {
		foreach (int c in itemCount) {
			if(c > 0) {
				return true;
			}
		}
		return false;
	}

	/** 現在の手持ちの個数 */
	public int getTotalInHand() {
		int total = 0;
		foreach (int c in itemCount) {
			total += c;
		}
		return total;
	}

	/** このワールドに存在するアイテムの個数 */
	public int getTotalInWorld() {
		int total = 0;
		foreach (int c in itemTotalCount) {
			total += c;
		}
		return total;
	}

	/** カゴの中にあるアイテムの個数 */
	public int getTotalInCarrier() {
		int total = 0;
		foreach (int c in itemInCarrier) {
			total += c;
		}
		return total;
	}

	/** 全てカゴに果物を入れたかどうか */
	public bool isCompleted() {
		return getTotalInCarrier() == getTotalInWorld();
	}

	/** カゴに手持ちのアイテムを移す */
	public void itemToPoint() {
		for (int i = 0; i < itemCount.Length; i++) {
			itemInCarrier[i] += itemCount[i];
		}
		clear();
	}

	/** 手持ちのアイテムをロストさせる */
	public void clear() {
		for (int i = 0; i < itemCount.Length; i++) {
			itemCount[i] = 0;
		}
	}
}
