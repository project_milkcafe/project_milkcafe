﻿using UnityEngine;
using System.Collections;

/**
 * ゲームに必要な情報を保持する統治者です。
 */
public class GameMaster : MonoBehaviour {
	/** 統治者へアクセスするためのプロパティ */
	public static GameMaster instance { get; private set; }

	/** 位置に関する情報を保持するオブジェクトです。 */
	public PositionCollection  positionCollection;

	/** 取得したアイテムの情報を保持するオブジェクトです。 */
	public ScoreBoard scoreBoard;

	/** アイコンの画像を保持するオブジェクトです。 */
	public ItemIconTable itemIconTable;

	/** ゲームの時間を管理するオブジェクトです。 */
	public GameTimer gameTimer;

	/** ゲームのBGM、SEを鳴らすオブジェクトです*/
	public GameSound gameSound;

	void Awake() {
		instance = this;
	}
}
