﻿using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {
	/** スタートした時の、起動したときからの時刻 */
	public float startTime;

	/** スタートした瞬間と対応する経過時間 */
	public float timeOffset = 0.0f;

	/** 制限時間 */
	public float timeLimit = 90.0f;

	/** 残り時間 */
	public float currentTime = 0.0f;

	/** タイマーが動いているか否か */
	public bool isStarted = false;

	/** ゲームが終了しているか否か */
	public bool isEnded = false;

	/** タイマーを(再び)動かし始める */
	public void startGameTimer() {
		if (isStarted) {
			return;
		}
		if(isEnded){
			return;
		}
		isStarted = true;
		startTime = Time.realtimeSinceStartup;
	}

	/** タイマーを一時停止する */
	public void stopGameTimer() {
		if (!isStarted) {
			return;
		}
		isStarted = false;
		timeOffset += Time.realtimeSinceStartup - startTime;
	}

	/** タイマーの情報を更新する */
	void Update () {
		if (isStarted) {
			currentTime = timeLimit - (Time.realtimeSinceStartup - startTime) - timeOffset;
		}
		else {
			currentTime = timeLimit - timeOffset;
		}
		if (currentTime < 0) {
			isEnded = true;
			stopGameTimer();
		}
	}


}
