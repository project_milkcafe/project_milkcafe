﻿using UnityEngine;
using System.Collections;

public class DigButton : MonoBehaviour {
	public GameObject cutinPrefab;
	public float sensibility = 60.0f;
	public void OnClick() {
		float dist = GameMaster.instance.positionCollection.nearestItemDistance;
		if (dist < sensibility) {
			GameObject nearestItem = GameMaster.instance.positionCollection.nearestItem;
			BuriedItem item = nearestItem.GetComponent<BuriedItem>();
			item.appear();

			//GameObject cutin = (GameObject) Instantiate(cutinPrefab, Vector3.zero, Quaternion.identity);
			//cutin.transform.SetParent(transform.parent);
			//cutin.transform.localScale = Vector3.one;
		}
	}
}
