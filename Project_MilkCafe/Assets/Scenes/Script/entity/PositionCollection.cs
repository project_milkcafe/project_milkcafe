﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PositionCollection : MonoBehaviour {
	public GameObject player;
	public GameObject basket;
	public List<GameObject> items;

	public GameObject nearestItem { get; private set; }
	public float nearestItemDistance { get; private set; }
	public float itemCarrierDistance { get; private set; }

	void Start()
	{
		items = new List<GameObject>();
	}

	void Update()
	{
		nearestItem = null;
		nearestItemDistance = float.MaxValue;
		itemCarrierDistance = float.MaxValue;

		if (player == null) {
			return;
		}
		Vector3 playerPosition = player.transform.position;

		if (items.Count > 0) {
			foreach (GameObject item in items) {
				float distance = Vector3.Distance(playerPosition, item.transform.position);
				if (distance < nearestItemDistance) {
					nearestItemDistance = distance;
					nearestItem = item;
				}
			}
		}

		itemCarrierDistance = Vector3.Distance(playerPosition, basket.transform.position);
	}
}
