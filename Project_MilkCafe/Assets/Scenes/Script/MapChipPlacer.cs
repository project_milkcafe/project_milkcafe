﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapChipPlacer: MonoBehaviour {
	public GameObject[] mapChipPrefabs;
	public GameObject[] itemPrefabs;
	public GameObject[] treePrefabs;
	public GameObject[] wallPrefabs;
	public float mapchipWidth;	
	public float mapchipHeight;
	public float wallMargin;
	public float wallHeight;
	public GameObject wallLeft;
	public GameObject wallRight;
	public GameObject wallFront;
	public GameObject wallBack;


	private void Start()
	{
        // マップの生成
        int mapWidth = SceneBridge.mapWidth;
        int mapHeight = SceneBridge.mapHeight;
        float mapWidthInPixel = mapWidth * mapchipWidth;
		float mapHeightInPixel = mapHeight * mapchipHeight;
		wallLeft.GetComponent<BoxCollider>().size = new Vector3(wallMargin, wallHeight, mapHeightInPixel + wallMargin);
		wallRight.GetComponent<BoxCollider>().size = new Vector3(wallMargin, wallHeight, mapHeightInPixel + wallMargin);
		wallFront.GetComponent<BoxCollider>().size = new Vector3(mapWidthInPixel + wallMargin, wallHeight, wallMargin);
		wallBack.GetComponent<BoxCollider>().size = new Vector3(mapWidthInPixel + wallMargin, wallHeight, wallMargin);
		wallLeft.transform.position = new Vector3((-wallMargin - mapchipWidth) / 2, wallHeight / 2, (mapHeightInPixel + wallMargin) / 2 - mapchipHeight);
		wallRight.transform.position = new Vector3((wallMargin - mapchipWidth) / 2 + mapWidthInPixel, wallHeight / 2, (mapHeightInPixel + wallMargin) / 2 - mapchipHeight);
		wallFront.transform.position = new Vector3((mapWidthInPixel + wallMargin) / 2 - mapchipWidth, wallHeight / 2, (-wallMargin - mapchipHeight) / 2);
		wallBack.transform.position = new Vector3((mapWidthInPixel + wallMargin) / 2 - mapchipWidth, wallHeight / 2, (wallMargin - mapchipHeight) / 2 + mapHeightInPixel);


        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                int mapX = x;
				int mapY = mapHeight - y - 1;
				Vector3 pos = Vector3.right * x * mapchipWidth + Vector3.forward * y * mapchipHeight;

                switch (SceneBridge.mapData[mapY, mapX])
                {
                    // かご
                    case 0: {
                        // スタート地点の設定
                        Vector3 startPoint = new Vector3(mapX, mapY - 1, 0);
                        GameMaster.instance.positionCollection.player.transform.position = new Vector3(startPoint.x * mapchipWidth,	0.25f, (mapHeight - startPoint.y - 1) * mapchipHeight);
		                GameMaster.instance.positionCollection.basket.transform.position = new Vector3(startPoint.x * mapchipWidth,	9.0f,(mapHeight - startPoint.y - 1) * mapchipHeight - 25.0f);

                        // 地面は通常の草地
                        GameObject prefab = mapChipPrefabs[1];
                        GameObject mapChip = (GameObject)Instantiate(prefab, pos, prefab.transform.rotation);
                        mapChip.transform.SetParent(transform);
                        break;
                    }

                    // 通常の草地
                    case 1: {
                        GameObject prefab = mapChipPrefabs[1];
                        GameObject mapChip = (GameObject)Instantiate(prefab, pos, prefab.transform.rotation);
                        mapChip.transform.SetParent(transform);
                        break;
                    }

                    // 減速する草地
                    case 2: {
                        GameObject prefab = mapChipPrefabs[2];
                        GameObject mapChip = (GameObject)Instantiate(prefab, pos, prefab.transform.rotation);
                        mapChip.transform.SetParent(transform);
                        break;
                    }

                    // 壁
                    case 3: {
                        // 地面の設定
                        GameObject prefab = mapChipPrefabs[2];
                        GameObject mapChip = (GameObject)Instantiate(prefab, pos, prefab.transform.rotation);
                        mapChip.transform.SetParent(transform);

                        // 壁オブジェクトの設定
                        GameObject prefab2 = wallPrefabs[1];
                        GameObject wallObject = (GameObject)Instantiate(prefab2, pos, prefab2.transform.rotation);
                        wallObject.transform.SetParent(transform);
                        // もし一番手前の壁であるかもしくは手前側に壁があれば手前の壁をDestroyする。
                        if (mapY == mapHeight - 1 || SceneBridge.mapData[mapY + 1, mapX] == 3)
                        {
                            Destroy(wallObject.transform.FindChild("front").gameObject);
                        }
                        // もし一番奥の壁であるかもしくは奥側に壁があれば奥の壁をDestroyする。
                        if (mapY == 0 || SceneBridge.mapData[mapY - 1, mapX] == 3)
                        {
                            Destroy(wallObject.transform.FindChild("back").gameObject);
                        }
                        // もし一番左側の壁であるかもしくは左側に壁があれば左の壁をDestroyする。
                        if (mapX == 0 || SceneBridge.mapData[mapY, mapX - 1] == 3)
                        {
                            Destroy(wallObject.transform.FindChild("left").gameObject);
                        }
                        // もし一番右側の壁であるかもしくは右側に壁があれば右の壁をDestroyする。
                        if (mapX == mapWidth - 1 || SceneBridge.mapData[mapY, mapX + 1] == 3)
                        {
                            Destroy(wallObject.transform.FindChild("right").gameObject);
                        }
                        break;
                    }

                    // 木
                    case 4: {
                        // 床
                        GameObject prefab = mapChipPrefabs[2];
                        GameObject mapChip = (GameObject)Instantiate(prefab, pos, prefab.transform.rotation);
                        mapChip.transform.SetParent(transform);

                        // 木
                        GameObject prefab2 = treePrefabs[1];
                        GameObject treeObject = (GameObject)Instantiate(prefab2, pos, prefab2.transform.rotation);
                        treeObject.transform.SetParent(transform);
                        break;
                    }

                    // フルーツ
                    case 5: // イチゴ
                    case 6: // ブドウ
                    case 7: // オレンジ
                    case 8: // メロン
                    case 9: // バナナ
                        {
                        // 床
                        GameObject prefab = mapChipPrefabs[1];
                        GameObject mapChip = (GameObject)Instantiate(prefab, pos, prefab.transform.rotation);
                        mapChip.transform.SetParent(transform);

                        // アイテム
                        GameObject prefab2 = itemPrefabs[SceneBridge.mapData[mapY, mapX] - 4];
                        GameObject itemChip = (GameObject)Instantiate(prefab2, pos + Vector3.up * 10, prefab2.transform.rotation);
                        itemChip.transform.SetParent(transform);
                        itemChip.GetComponent<SpriteRenderer>().enabled = false;
                        BuriedItem buriedItem = itemChip.GetComponent<BuriedItem>();
                        buriedItem.kind = SceneBridge.mapData[mapY, mapX] - 4;
                        GameMaster.instance.scoreBoard.itemTotalCount[SceneBridge.mapData[mapY, mapX] - 4]++;
                        break;
                    }

                    default:
                        Debug.Log("不正なマップチップデータ\n" + "mapData[" + y + "," + x + "] = " + SceneBridge.mapData[y, x]);
                        break;
                }

            }
        }

	}


}