﻿using UnityEngine;
using System.Collections;

public class MapLoader {
    private TextAsset mapData; // マップデータの保持用 
	
    // マップデータの読み込み
    public void load(string path) {
		mapData = (TextAsset)Resources.Load(path);
		Debug.Log(mapData.text);
	}

    // マップデータの取得
    public int[,] getMapData()
    {
        // 
        string[] rows = mapData.text.Replace("\r\n", "\n").Split('\n');
        int width = rows[0].Split(',').Length; // マップの横幅
        int height = rows.Length - 1;          // マップの縦幅

        int[,] data = new int[height, width];
        for (int y = 0; y < height; y++)
        {
            string[] elements = rows[y].Split(',');
            for(int x = 0; x < width; x++){
                data[y, x] = int.Parse(elements[x]);
            }
        }

        return data;
    }
}