﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BacketSarcher : MonoBehaviour{
	public float distance = 100;
	public bool isLeft;

	Vector3 vecA,vecB;
	float cos, sin;

	void Start(){
		
	}

	void Update(){
		PositionCollection pos = GameMaster.instance.positionCollection;
		Vector3 vecA3 = pos.player.transform.forward;
		Vector3 vecB3 = pos.basket.transform.position - pos.player.transform.position;

		//ミルクの向きのベクトル
		vecA = new Vector2(vecA3.x, vecA3.z);
		//ミルクからカゴの向きのベクトル
		vecB = new Vector2(vecB3.x, vecB3.z);

		if (vecA.magnitude <= 0 && vecB.magnitude <= 0) {
			// TODO: 矢印を表示しない処理
			return;
		}

		cos = Vector2.Dot(vecA, vecB) / (vecA.magnitude * vecB.magnitude);
		if (vecB.magnitude < distance || cos >= Mathf.Sqrt(1/2) ) {
			this.GetComponent<Image>().enabled = false;
		}
		else {
			sin = vecA.x * vecB.y - vecB.x * vecA.y;
			if (sin > 0 && isLeft) {
				this.GetComponent<Image>().enabled = true;
			}
			else if (sin < 0 && !isLeft) {
				this.GetComponent<Image>().enabled = true;
			}
			else {
				this.GetComponent<Image>().enabled = false;
			}
		}
		//Debug.Log(sin);
	}


}
