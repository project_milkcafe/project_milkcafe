﻿using UnityEngine;
using System.Collections;

public class ItemCompleteChecker : MonoBehaviour {
	public bool isCompleted = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isCompleted) {
			return;
		}
		if(GameMaster.instance.scoreBoard.isCompleted()){
			isCompleted = true;
			GameMaster.instance.gameTimer.isEnded = true;
		}
	}
}