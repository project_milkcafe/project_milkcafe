﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
	public void OnClickContinue() {
		Debug.Log("OnClickContinue");
		GameMaster.instance.gameTimer.startGameTimer();
		Destroy(this.gameObject);
	}

	public void OnClickRetry() {
		Debug.Log("OnClickRetry");
		GameMaster.instance.gameTimer.startGameTimer();
		SceneManager.LoadScene("GamePlayScene"); // シーンの名前かインデックスを指定
	}

	public void OnClickRetire() {
		SceneManager.LoadScene("MenuScene");
		Debug.Log("OnClickRetire");
	}
}

