﻿using UnityEngine;
using System.Collections;

public class DashController : MonoBehaviour {
	public bool isTouchMode = true;                   // タッチかクリックか
	public float touchSensibility = 0.0125f;          // タッチ感度
	public float velocitySensibility = 1.0f;          // 速度感度
	public float straightAngleLimit = 22.5f;          // 平角限度
	public float rotateAndRunAngleLimit = 60.0f;      // 
	public float rotateOnlyAngleLimit = 112.5f;       // 
	public float backAngleLimit = 60.0f;              // 
	public float runSpeed = 0.5f;                     // ダッシュ速度
	public float backSpeed = 0.4f;                    // 後退速度
	public float rotateSpeed = 0.0002f;               // 回転速度
	public float brakeRate = 0.90f;                   // 
	public float maxSpeed = 220.0f;                   // 最高速度
	public float minSpeed = 110.0f;                   // 最低速度
	public float weightRate = 1.0f;                   // フルーツ一個当たりの重さ
	public float weightOffset = 7.0f;                 // 重さの基準値
     
    private Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}


	void FixedUpdate ()
	{
		rb.mass = GameMaster.instance.scoreBoard.getTotalInHand() * weightRate + weightOffset;

		if (GameMaster.instance.gameTimer.isStarted) {
			// true : smartphone, false : PC
			if (isTouchMode) {
				for (int i = 0; i < Input.touchCount; i++) {
					Touch touch = Input.GetTouch(i);
					if (touch.phase == TouchPhase.Moved) {
						float deltaTime = touch.deltaTime;
						if (deltaTime < 1 / 60.0f) {
							deltaTime = 1 / 60.0f;
						}

						Move(
							new Vector2(
								touch.deltaPosition.x,
								touch.deltaPosition.y
							),
							deltaTime
						);

					}
				}
			}
			else {
				if (Input.GetMouseButton(0)) {
					Move(
						new Vector2(
							Input.GetAxis("Mouse X"),
							Input.GetAxis("Mouse Y")
						) * 10.0f,
						Time.deltaTime
					);
				}
			}
		}
	}



    // -----------------------------------------------------------------------------
    /// <summary>
    ///  移動処理を行う関数
    /// </summary>
    /// <param name="difference">タッチの移動距離</param>
    /// <param name="deltaTime"></param>

	void Move(Vector2 difference, float deltaTime)
	{
		if (difference.sqrMagnitude < touchSensibility * touchSensibility) {
			return;
		}
		difference /= -deltaTime * Screen.height / 100.0f;
		Vector3 force = new Vector3(-difference.x, 0.0f, difference.y);

		Vector3 velocityWithoutGrabity = rb.velocity;
		velocityWithoutGrabity.y = 0.0f;
		Vector3 relativeVelocity = transform.InverseTransformDirection(velocityWithoutGrabity);
		bool isMinimumVelocity = relativeVelocity.sqrMagnitude <= velocitySensibility * velocitySensibility;

		float touchAngle = Mathf.Atan2(-difference.x, difference.y) * Mathf.Rad2Deg;
		float touchAbsAngle = Mathf.Abs(touchAngle);
		float touchHorizontalDirection = touchAngle >= 0 ? 1 : -1;

		// forward
		if (Mathf.Abs(touchAngle) <= rotateOnlyAngleLimit) {
			// straight
			if (touchAbsAngle < straightAngleLimit) {
				rb.AddRelativeForce(Vector3.forward * difference.magnitude * runSpeed, ForceMode.Impulse);
			}
			// rotate and run
			else if (touchAbsAngle < rotateAndRunAngleLimit) {
				float forceAngle = Vector3.Angle(isMinimumVelocity ? Vector3.forward : relativeVelocity, force);
				float rate = (touchAbsAngle - straightAngleLimit) / (rotateAndRunAngleLimit - straightAngleLimit);
				float forceAmount = force.magnitude * runSpeed * (1.0f - rate);
				rb.AddRelativeForce(Vector3.forward * forceAmount, ForceMode.Impulse);

				float angleAmount = force.magnitude * rotateSpeed * rate;
				Vector3 angle = new Vector3(0, angleAmount * -touchHorizontalDirection, 0);
				rb.rotation *= Quaternion.Euler(angle);
				rb.angularVelocity += angle;
			}
			// rotate only
			else {
				Vector3 angle = new Vector3(0, force.magnitude * rotateSpeed * -touchHorizontalDirection, 0);
				rb.rotation *= Quaternion.Euler(angle);
				rb.velocity = Quaternion.Euler(angle * 0.5f) * rb.velocity;
				rb.angularVelocity += angle;
			}
		}
		// back side
		else {
			float forceAngle = Vector3.Angle(isMinimumVelocity ? Vector3.back : relativeVelocity, force);

			// brake
			if (forceAngle >= 90.0f) {
				rb.velocity *= brakeRate;
			}
			// back
			else if (180.0f - Mathf.Abs(touchAngle) <= backAngleLimit) {
				rb.AddRelativeForce(Vector3.back * difference.magnitude * runSpeed, ForceMode.Impulse);
			}
		}

		if(rb.velocity.sqrMagnitude > maxSpeed * maxSpeed) {
			rb.velocity *= 0.70f + maxSpeed / rb.velocity.magnitude * 0.25f;
		}
	}
}
