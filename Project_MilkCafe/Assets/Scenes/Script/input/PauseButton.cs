﻿using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour {
	public GameObject pauseMenuPrefab;
	public GameObject statusDisplay;

	public void OnClick() {
		if (GameMaster.instance.gameTimer.isStarted) {
			//ゲームタイマーが止まる。
			//半透明の背景になる。
			//一時停止のSE再生
			GameMaster.instance.gameTimer.stopGameTimer();
			GameObject pauseMenu = (GameObject) Instantiate(pauseMenuPrefab, pauseMenuPrefab.transform.position, pauseMenuPrefab.transform.rotation);
			pauseMenu.transform.SetParent(statusDisplay.transform, false);
			AudioSource se = GetComponent<AudioSource>();
			AudioSource testbgm = GetComponent<AudioSource>();
			testbgm.Play();
		}
	}
}