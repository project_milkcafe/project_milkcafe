﻿using UnityEngine;
using System.Collections;

public class BGMplayer : MonoBehaviour {
	private AudioSource bgm;

	void Start(){
		/*AudioSourceのbgmからGetComponentする。**/
		bgm = GetComponent<AudioSource>();
	}
	void Update() {
		/*GameMasterからisStartedを呼び出す**/
		if (GameMaster.instance.gameTimer.isStarted) {
			/*もしbgmが再生中でないならbgmを再生する**/
			if (bgm.isPlaying == false) {
				bgm.Play();
			}
		}
		else {
			/*もしbgmが再生中でないならbgmを再生する**/
			if (bgm.isPlaying == true) {
				bgm.Pause();
			}
		}
	}
}
