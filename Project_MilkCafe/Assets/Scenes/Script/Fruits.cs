﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//フルーツの半透明・透明を設定します
public class Fruits : MonoBehaviour {
	//unity上のフルーツのゲームオブジェクト
	public Image[] strawberries;
	public Image[] grapes;
	public Image[] melons;
	public Image[] oranges;
	public Image[] bananas;

	// Use this for initialization
	void Start () {
		//イチゴの表示
		AlphaFruits (strawberries, GameMaster.instance.scoreBoard.itemInCarrier[1], GameMaster.instance.scoreBoard.itemTotalCount[1]);
		//グレープの表示
		AlphaFruits(grapes, GameMaster.instance.scoreBoard.itemInCarrier[2], GameMaster.instance.scoreBoard.itemTotalCount[2]);
		//メロンの表示
		AlphaFruits(melons, GameMaster.instance.scoreBoard.itemInCarrier[3], GameMaster.instance.scoreBoard.itemTotalCount[3]);
		//オレンジの表示
		AlphaFruits(oranges, GameMaster.instance.scoreBoard.itemInCarrier[4], GameMaster.instance.scoreBoard.itemTotalCount[4]);
		//バナナの表示
		AlphaFruits(bananas, GameMaster.instance.scoreBoard.itemInCarrier[5], GameMaster.instance.scoreBoard.itemTotalCount[5]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//フルーツの表示状態を設定します
	private void AlphaFruits(
		Image[] images, //フルーツオブジェクトの配列
		int object_count, //フルーツの獲得数
		int total_count //ワールド内に存在するフルーツの個数
	) {
		for (int i = 0; i < images.Length; i++) {
			//i番目のフルーツの画像を取得します
			Image image = images [i];
			//i + 1個以上フルーツを獲得していなければ、半透明になる
			if (object_count < i + 1) {
				image.color = new UnityEngine.Color(1, 1, 1, 0.5f);
				//i + 1個以上ワールド内にフルーツが存在していなければ、完全に透明になる
				if(total_count < i + 1){
					image.color = new UnityEngine.Color(1, 1, 1, 0.0f);
				}
			}
		}
	}
}
