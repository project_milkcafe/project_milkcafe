﻿using UnityEngine;
using System.Collections;

public class ItemCarrier : MonoBehaviour {
	public float sensibility = 60.0f;
	public GameObject statusCanvas;
	public GameObject cutIn;

	// Update is called once per frame
	void Update () {
		float dist = GameMaster.instance.positionCollection.itemCarrierDistance;
		if (dist < sensibility && GameMaster.instance.scoreBoard.hasItem()){
			GameMaster.instance.scoreBoard.itemToPoint();
			GameObject animation = (GameObject) Instantiate(cutIn, cutIn.transform.position, cutIn.transform.rotation);
			animation.transform.SetParent(statusCanvas.transform, false);
		}
	}
}