﻿using UnityEngine;
using System.Collections;

public class BillBoard : MonoBehaviour {
	public Camera targetCamera;
	public bool enabledZBillBoard = false;

	void Start ()
	{
		if (this.targetCamera == null)
			targetCamera = Camera.main;
	}

	void LateUpdate ()
	{
		if (enabledZBillBoard) {
			transform.right = targetCamera.transform.right;
		}
		else {
			transform.rotation = targetCamera.transform.rotation;
		}
	}
}
