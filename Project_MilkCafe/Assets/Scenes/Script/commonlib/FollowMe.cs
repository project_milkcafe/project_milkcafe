﻿using UnityEngine;
using System.Collections;

public class FollowMe : MonoBehaviour {

	public GameObject follower;
	public Vector3 diff;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		follower.transform.position = transform.position + diff;
	}
}
