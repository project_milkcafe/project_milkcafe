﻿using UnityEngine;
using System.Collections;

public class SelfDestroyer : MonoBehaviour {
	public void DestroyMyself()
	{
		Destroy(gameObject);
	}
}
