﻿using UnityEngine;
using System.Collections;

public class FollowingCamera : MonoBehaviour {
	public GameObject target;
	public Vector3 position;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		var targetTransform = target.transform;
		transform.position = targetTransform.position + targetTransform.rotation * position;
		transform.LookAt(targetTransform.position);
	}
}
