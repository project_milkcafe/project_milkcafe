﻿using UnityEngine;
using System.Collections;

public class LookMe : MonoBehaviour {
	public GameObject follower;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		follower.transform.LookAt(transform.position);
	}
}
