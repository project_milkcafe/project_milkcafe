﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleTransparenter : MonoBehaviour {
	public GameObject target;
	public LayerMask layer;
	public float alphaOffset;
	public float extraRayLength;
	public float transparentRate;

	private Dictionary<Renderer, Color> prevColor = new Dictionary<Renderer, Color>();
	private HashSet<Renderer> nextObstacles = new HashSet<Renderer>();
	private HashSet<Renderer> respawnObstacles = new HashSet<Renderer>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (target == null) {
			return;
		}

		Vector3 a = transform.position;
		Vector3 b = target.transform.position;
		Vector3 sub = (b - a).normalized;
		float dist = Vector3.Distance(a, b);

		RaycastHit[] hits = Physics.RaycastAll(a - sub * extraRayLength, sub, dist + extraRayLength, layer.value);
		foreach (RaycastHit hit in hits) {
			Renderer r = (Renderer) hit.transform.GetComponent<Renderer>();
			if (nextObstacles.Contains(r)) {
				continue;
			}
			nextObstacles.Add(r);

			Color color = r.material.color;
			if (!prevColor.ContainsKey(r)) {
				prevColor.Add(r, color);
			}

			// TODO: 特許期限切れ (2016/5/15) になったらフラグを入れ替える
			if (true) {
				color.a *= transparentRate;
				r.material.color = color;
			}
			else {
				float hitDistance = hit.distance - extraRayLength;
				if (hitDistance < 0.0f) hitDistance = 0.0f;
				if (prevColor.TryGetValue(r, out color)) {
					color.a *= (hitDistance / dist) * (1.0f - alphaOffset) + alphaOffset;
					r.material.color = color;
				}
			}
		}

		foreach (var entry in prevColor) {
			if (!nextObstacles.Contains(entry.Key)) {
				entry.Key.material.color = entry.Value;
				respawnObstacles.Add(entry.Key);
			}
		}

		foreach (var r in respawnObstacles) {
			prevColor.Remove(r);
		}

		nextObstacles.Clear();
		respawnObstacles.Clear();
	}
}
