﻿using UnityEngine;
using System.Collections;

public class Fog : MonoBehaviour {
	// Use this for initialization
	void Start () {
		RenderSettings.fog = true;
		RenderSettings.fogStartDistance = 500.0f;
		RenderSettings.fogEndDistance = 1000.0f;
		RenderSettings.fogColor = new Color(0.5f, 1.0f, 1.0f);
		RenderSettings.fogMode = FogMode.Linear;
	}
}
