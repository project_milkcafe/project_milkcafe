﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Logger : MonoBehaviour {
	public static void Log(params string[] texts)
	{
		instance.Log(string.Join(" ", texts));
	}

	public static void Clear()
	{
		instance.Clear();
	}

	public static int length {
		get { return instance._maxLogs; }
		set { instance._maxLogs = value; }
	}

	public GUIStyle foregroundStyle = new GUIStyle();
	public GUIStyle backgroundStyle = new GUIStyle();

	void OnGUI () {
		Rect area1 = new Rect(20, 20, Screen.width - 40, Screen.height - 40);
		Rect area2 = new Rect(21, 21, Screen.width - 40, Screen.height - 40);
		string log = instance.ToString();
		GUI.Label(area2, log, backgroundStyle);
		GUI.Label(area1, log, foregroundStyle);
	}



	public class Impl
	{
		internal void Log(string text)
		{
			_logs.Insert(0, text);
			if (_logs.Count > _maxLogs) {
				_logs.RemoveRange(_maxLogs, _logs.Count - _maxLogs);
			}
			_isDirtyLogsCache = true;
		}
		
		internal void Clear()
		{
			_logs.Clear();
			_isDirtyLogsCache = true;
		}
		
		public override string ToString()
		{
			if (_isDirtyLogsCache) {
				_logsCache = string.Join("\n", _logs.ToArray());
				_isDirtyLogsCache = false;
			}
			return _logsCache;
		}
		
		public int _maxLogs = 10;
		private List<string> _logs = new List<string>();
		private string _logsCache = "";
		private bool _isDirtyLogsCache = false;
	}
	
	private static Impl instance {
		get {
			if (_instance == null) {
				_instance = new Impl();
			}
			return _instance;
		}
	}
	private static Impl _instance;
}
