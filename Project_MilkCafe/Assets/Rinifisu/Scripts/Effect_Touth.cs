﻿using UnityEngine;
using System.Collections;

public class Effect_Touth : MonoBehaviour
{
    public GameObject Effect;
    public int CreateValue;

    public float MoveRange_Min;
    public float MoveRange_Max;
    public float DestroyTime;

    void Start()
    {
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
    }

    void OnDestroy()
    {
        for (int i = 0; i < CreateValue; ++i)
        {
            GameObject CreateEffect = Instantiate(Effect);

            CreateEffect.transform.SetParent(transform.parent.transform, false);
            CreateEffect.transform.localPosition = transform.localPosition;

            Vector3 normal = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0.0f).normalized;
            CreateEffect.GetComponent<AutoMoveVector>().MoveStart(transform.localPosition, transform.localPosition + normal * Random.Range(MoveRange_Min, MoveRange_Max), DestroyTime, MoveEndAction.None);
            CreateEffect.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));
        }
    }
}
