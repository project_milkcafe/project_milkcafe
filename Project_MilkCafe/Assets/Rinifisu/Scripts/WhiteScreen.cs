﻿using UnityEngine;
using System.Collections;

public class WhiteScreen : MonoBehaviour
{
    public bool isActive = true;

    public float MoveTime = 2.0f;
    public float TimeSecond = 2.0f;

    public bool isWhiteIn = true;

    void Update()
    {
        if (isActive)
        {
            if (isWhiteIn)
            {
                TimeSecond -= Time.deltaTime;

                if (TimeSecond <= 0.0f)
                {
                    TimeSecond = 0.0f;
                    isActive = false;
                }
            }
            else
            {
                TimeSecond += Time.deltaTime;

                if (MoveTime <= TimeSecond)
                {
                    TimeSecond = MoveTime;
                    isActive = false;
                }
            }
        }

        GetComponent<CanvasRenderer>().SetColor(new Color(1.0f, 1.0f, 1.0f, TimeSecond / MoveTime));
    }
}
