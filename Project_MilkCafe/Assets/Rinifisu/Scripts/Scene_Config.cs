﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;

enum SceneState_Config
{
    ShowConfig,
    Wait_Button,
    Wait_AllHide,

    SceneChange,

    HowToPlay_Show,
    HowToPlay_Wait,
    HowToPlay_LeftMove,
    HowToPlay_RightMove,
    HowToPlay_Hide,

    StaffCredit_Wait,
}

public class Scene_Config : MonoBehaviour
{
    public bool isActive;

    public GameObject Button_HowToPlay;
    public GameObject Button_Gallery;
    public GameObject Button_Story;
    public GameObject Button_StaffCredit;
    public GameObject Button_Back;

    public AutoMoveVector StaffCredit_Background;

    public GameObject HowToPlay_Background_A;
    public GameObject HowToPlay_Background_B;
    public GameObject HowToPlay_Button_Left;
    public GameObject HowToPlay_Button_Right;
    public GameObject HowToPlay_Button_Back;
    public List<Sprite> HowToPlay_Sprite;

    private int ActivePage;

    private SceneState_Config State;
    private SceneState_Config State_Old;

    private bool State_Once;

    void Awake()
    {
        isActive = false;

        State = SceneState_Config.ShowConfig;
        State_Once = true;
    }

    void SceneChange(SceneState_Config state)
    {
        State = state;
        State_Once = true;
    }

    bool MoveUpdate()
    {
        if (State_Once)
        {
            Button_HowToPlay.GetComponent<AutoMoveVector>().ActiveFlag = true;
            Button_Gallery.GetComponent<AutoMoveVector>().ActiveFlag = true;
            Button_Story.GetComponent<AutoMoveVector>().ActiveFlag = true;
            Button_StaffCredit.GetComponent<AutoMoveVector>().ActiveFlag = true;
            Button_Back.GetComponent<AutoMoveVector>().ActiveFlag = true;
        }

        return !Button_HowToPlay.GetComponent<AutoMoveVector>().ActiveFlag
        && !Button_Gallery.GetComponent<AutoMoveVector>().ActiveFlag
        && !Button_Story.GetComponent<AutoMoveVector>().ActiveFlag
        && !Button_StaffCredit.GetComponent<AutoMoveVector>().ActiveFlag
        && !Button_Back.GetComponent<AutoMoveVector>().ActiveFlag;
    }

    void Update()
    {
        if (!isActive) return;

        State_Old = State;

        switch (State)
        {
            case SceneState_Config.ShowConfig:
                {
                    if (MoveUpdate())
                    {
                        SceneChange(SceneState_Config.Wait_Button);
                    }
                }
                break;

            case SceneState_Config.Wait_Button:
                {
                    if (Button_HowToPlay.GetComponent<MouseChecker>().isClick && HowToPlay_Sprite.Count != 0)
                    {
                        SceneChange(SceneState_Config.HowToPlay_Show);
                    }
                    else if (Button_Gallery.GetComponent<MouseChecker>().isClick)
                    {
                        //SceneChange(SceneState_Config.Wait_AllHide);
                    }
                    else if (Button_Story.GetComponent<MouseChecker>().isClick)
                    {
                        //SceneChange(SceneState_Config.Wait_AllHide);
                    }
                    else if (Button_StaffCredit.GetComponent<MouseChecker>().isClick)
                    {
                        SceneChange(SceneState_Config.StaffCredit_Wait);
                    }
                    else if (Button_Back.GetComponent<MouseChecker>().isClick)
                    {
                        SceneChange(SceneState_Config.Wait_AllHide);
                    }
                }
                break;

            case SceneState_Config.Wait_AllHide:
                {
                    if (MoveUpdate())
                    {
                        SceneChange(SceneState_Config.SceneChange);
                    }
                }
                break;

            case SceneState_Config.SceneChange:
                {
                    if (State_Once)
                    {
                        GetComponent<Scene_Menu>().isActive = true;
                        isActive = false;

                        SceneChange(SceneState_Config.ShowConfig);
                    }
                }
                break;

            case SceneState_Config.HowToPlay_Show:
                {
                    if (State_Once)
                    {
                        ActivePage = 0;

                        HowToPlay_Background_A.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage];
                        HowToPlay_Background_B.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage];

                        HowToPlay_Button_Left.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        HowToPlay_Button_Right.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        HowToPlay_Button_Back.GetComponent<AutoMoveVector>().ActiveFlag = true;

                        HowToPlay_Background_B.GetComponent<AutoMoveVector>().ActiveFlag = true;

                        MoveUpdate();
                    }

                    if (!HowToPlay_Background_B.GetComponent<AutoMoveVector>().ActiveFlag)
                    {
                        HowToPlay_Background_A.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

                        SceneChange(SceneState_Config.HowToPlay_Wait);
                    }
                }
                break;

            case SceneState_Config.HowToPlay_Wait:
                {
                    if (HowToPlay_Button_Left.GetComponent<MouseChecker>().isClick && 0 < ActivePage)
                    {
                        SceneChange(SceneState_Config.HowToPlay_LeftMove);
                    }
                    else if (HowToPlay_Button_Right.GetComponent<MouseChecker>().isClick && ActivePage < HowToPlay_Sprite.Count - 1)
                    {
                        SceneChange(SceneState_Config.HowToPlay_RightMove);
                    }
                    else if (HowToPlay_Button_Back.GetComponent<MouseChecker>().isClick)
                    {
                        SceneChange(SceneState_Config.HowToPlay_Hide);
                    }
                }
                break;

            case SceneState_Config.HowToPlay_LeftMove:
                {
                    if (State_Once)
                    {
                        --ActivePage;

                        HowToPlay_Background_A.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage + 1];
                        HowToPlay_Background_A.GetComponent<AutoMoveVector>().EndVector = new Vector3(1000.0f, 80.0f, 0.0f);
                        HowToPlay_Background_A.GetComponent<AutoMoveVector>().ActiveFlag = true;

                        HowToPlay_Background_B.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage];
                        HowToPlay_Background_B.GetComponent<AutoMoveVector>().StartVector = new Vector3(-1000.0f, 80.0f, 0.0f);
                        HowToPlay_Background_B.GetComponent<AutoMoveVector>().ActiveFlag = true;
                    }

                    if (!HowToPlay_Background_A.GetComponent<AutoMoveVector>().ActiveFlag
                        && !HowToPlay_Background_B.GetComponent<AutoMoveVector>().ActiveFlag)
                    {
                        HowToPlay_Background_A.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage];

                        SceneChange(SceneState_Config.HowToPlay_Wait);
                    }
                }
                break;

            case SceneState_Config.HowToPlay_RightMove:
                {
                    if (State_Once)
                    {
                        ++ActivePage;

                        HowToPlay_Background_A.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage - 1];
                        HowToPlay_Background_A.GetComponent<AutoMoveVector>().EndVector = new Vector3(-1000.0f, 80.0f, 0.0f);
                        HowToPlay_Background_A.GetComponent<AutoMoveVector>().ActiveFlag = true;

                        HowToPlay_Background_B.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage];
                        HowToPlay_Background_B.GetComponent<AutoMoveVector>().StartVector = new Vector3(1000.0f, 80.0f, 0.0f);
                        HowToPlay_Background_B.GetComponent<AutoMoveVector>().ActiveFlag = true;
                    }

                    if (!HowToPlay_Background_A.GetComponent<AutoMoveVector>().ActiveFlag
                        && !HowToPlay_Background_B.GetComponent<AutoMoveVector>().ActiveFlag)
                    {
                        HowToPlay_Background_A.GetComponent<Image>().sprite = HowToPlay_Sprite[ActivePage];

                        SceneChange(SceneState_Config.HowToPlay_Wait);
                    }
                }
                break;

            case SceneState_Config.HowToPlay_Hide:
                {
                    if (State_Once)
                    {
                        HowToPlay_Background_A.GetComponent<AutoMoveVector>().EndVector = new Vector3(1000.0f, 80.0f, 0.0f);
                        HowToPlay_Background_A.GetComponent<AutoMoveVector>().ActiveFlag = true;

                        HowToPlay_Button_Left.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        HowToPlay_Button_Right.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        HowToPlay_Button_Back.GetComponent<AutoMoveVector>().ActiveFlag = true;
                    }

                    if (MoveUpdate() && !HowToPlay_Background_A.GetComponent<AutoMoveVector>().ActiveFlag)
                    {
                        HowToPlay_Background_A.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

                        SceneChange(SceneState_Config.Wait_Button);
                    }
                }
                break;

            case SceneState_Config.StaffCredit_Wait:
                {
                    if (State_Once)
                    {
                        StaffCredit_Background.ActiveFlag = true;

                        MoveUpdate();
                    }

                    if (!StaffCredit_Background.ActiveFlag && Input.GetMouseButton(0))
                    {
                        StaffCredit_Background.ActiveFlag = true;

                        SceneChange(SceneState_Config.ShowConfig);
                    }
                }
                break;
        }

        if (State_Old == State) State_Once = false;
    }
}