﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

enum SceneState_Menu
{
    ShowMenu,
    ShowIcon_and_Text,
    Wait_Button,
    Wait_AllHide,

    Back_SceneChange,

    OrderSelect_ZoomIn,
    OrderSelect_SceneChange,
    OrderSelect_ZoomOut,

    Config_OpenDoor_and_ZoomIn,
    Config_SceneChange,
    Config_CloseDoor_and_ZoomOut,
}

public class Scene_Menu : MonoBehaviour
{
    public bool isActive;

    public AutoMoveVector Icon;
    public AutoMoveVector Text_Back;

    public string Serif;

    public GameObject Button_Back;
    public GameObject Button_OrderSelect;
    public GameObject Button_Config;

    public GameObject OrderSelect_MoveCamera_Target;

    public AutoMoveVector Config_Door;
    public GameObject Config_MoveCamera_Target;

    public GameObject ZoomOut_MoveCamera_Target;

    public AutoMoveVector MainCamera;

    private SceneState_Menu State;
    private SceneState_Menu State_Old;

    private SceneState_Menu State_Next;

    private bool State_Once;

    void Awake()
    {
        isActive = false;

        State = SceneState_Menu.ShowMenu;
        State_Once = true;
    }

    void SceneChange(SceneState_Menu state)
    {
        State = state;
        State_Once = true;
    }

    void Update()
    {
        if (!isActive) return;

        State_Old = State;

        switch (State)
        {
            case SceneState_Menu.ShowMenu:
                {
                    if (State_Once)
                    {
                        Button_Back.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        Button_OrderSelect.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        Button_Config.GetComponent<AutoMoveVector>().ActiveFlag = true;
                    }

                    if (!Button_Back.GetComponent<AutoMoveVector>().ActiveFlag
                    && !Button_OrderSelect.GetComponent<AutoMoveVector>().ActiveFlag
                    && !Button_Config.GetComponent<AutoMoveVector>().ActiveFlag)
                    {
                        SceneChange(SceneState_Menu.ShowIcon_and_Text);
                    }
                }
                break;

            case SceneState_Menu.ShowIcon_and_Text:
                {
                    if (State_Once)
                    {
                        Icon.ActiveFlag = true;
                        Text_Back.ActiveFlag = true;
                        Text_Back.transform.FindChild("Serif").GetComponent<Text>().text = Serif;

                        SceneChange(SceneState_Menu.Wait_Button);
                    }
                }
                break;

            case SceneState_Menu.Wait_Button:
                {
                    if (Button_Back.GetComponent<MouseChecker>().isClick)
                    {
                        State_Next = SceneState_Menu.Back_SceneChange;
                    }
                    else if (Button_OrderSelect.GetComponent<MouseChecker>().isClick)
                    {
                        State_Next = SceneState_Menu.OrderSelect_ZoomIn;
                    }
                    else if (Button_Config.GetComponent<MouseChecker>().isClick)
                    {
                        State_Next = SceneState_Menu.Config_OpenDoor_and_ZoomIn;
                    }
                    else break;

                    SceneChange(SceneState_Menu.Wait_AllHide);
                }
                break;

            case SceneState_Menu.Wait_AllHide:
                {
                    if (State_Once)
                    {
                        Button_Back.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        Button_OrderSelect.GetComponent<AutoMoveVector>().ActiveFlag = true;
                        Button_Config.GetComponent<AutoMoveVector>().ActiveFlag = true;

                        if (Icon.ActiveFlag) Icon.isBack = true;
                        if (Text_Back.ActiveFlag) Text_Back.isBack = true;

                        Icon.ActiveFlag = true;
                        Text_Back.ActiveFlag = true;
                    }

                    if (!Button_Back.GetComponent<AutoMoveVector>().ActiveFlag
                    && !Button_OrderSelect.GetComponent<AutoMoveVector>().ActiveFlag
                    && !Button_Config.GetComponent<AutoMoveVector>().ActiveFlag
                    && !Icon.ActiveFlag
                    && !Text_Back.ActiveFlag)
                    {
                        SceneChange(State_Next);
                    }
                }
                break;

            case SceneState_Menu.Back_SceneChange:
                {
                    if (State_Once)
                    {
                        GetComponent<Scene_Title>().isActive = true;
                        isActive = false;

                        SceneChange(SceneState_Menu.ShowMenu);
                    }
                }
                break;

            case SceneState_Menu.OrderSelect_ZoomIn:
                {
                    if (State_Once)
                    {
                        MainCamera.EndVector = OrderSelect_MoveCamera_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_Menu.OrderSelect_SceneChange);
                    }
                }
                break;

            case SceneState_Menu.OrderSelect_SceneChange:
                {
                    if (State_Once)
                    {
                        GetComponent<Scene_OrderSelect>().isActive = true;
                        isActive = false;

                        SceneChange(SceneState_Menu.OrderSelect_ZoomOut);
                    }
                }
                break;

            case SceneState_Menu.OrderSelect_ZoomOut:
                {
                    if (State_Once)
                    {
                        MainCamera.EndVector = ZoomOut_MoveCamera_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_Menu.ShowMenu);
                    }
                }
                break;

            case SceneState_Menu.Config_OpenDoor_and_ZoomIn:
                {
                    if (State_Once)
                    {
                        Config_Door.ActiveFlag = true;

                        MainCamera.EndVector = Config_MoveCamera_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_Menu.Config_SceneChange);
                    }
                }
                break;

            case SceneState_Menu.Config_SceneChange:
                {
                    if (State_Once)
                    {
                        GetComponent<Scene_Config>().isActive = true;
                        isActive = false;

                        SceneChange(SceneState_Menu.Config_CloseDoor_and_ZoomOut);
                    }
                }
                break;

            case SceneState_Menu.Config_CloseDoor_and_ZoomOut:
                {
                    if (State_Once)
                    {
                        Config_Door.ActiveFlag = true;

                        MainCamera.EndVector = ZoomOut_MoveCamera_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_Menu.ShowMenu);
                    }
                }
                break;
        }

        if (State_Old == State) State_Once = false;
    }
}
