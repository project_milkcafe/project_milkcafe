﻿using UnityEngine;

using UnityEngine.UI;

//自動設定する項目内容
public enum AutoSetFloat
{
    None,
    Position_X,
    Position_Y,
    Position_Z,
    Rotation_X,
    Rotation_Y,
    Rotation_Z,
    Scale_X,
    Scale_Y,
    Scale_Z,
    Scale,
    Color_Image_R,
    Color_Image_G,
    Color_Image_B,
    Color_Image_A,
    Color_Sprite_R,
    Color_Sprite_G,
    Color_Sprite_B,
    Color_Sprite_A,
    Color_Canvas_R,
    Color_Canvas_G,
    Color_Canvas_B,
    Color_Canvas_A,
}

//ベクトルの移動をサポートするクラス
public class AutoMoveFloat : MonoBehaviour
{
    //動作フラグ
    public bool ActiveFlag = false;
    //動作方面
    public bool isBack = false;

    //移動開始
    public float StartFloat;
    //移動終了
    public float EndFloat;

    //移動時間
    public float MoveTime = 0.0f;

    //曲線移動（上下無制限、左右 0.0～1.0 まで）
    public AnimationCurve Curve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    //自動設定する項目内容
    public AutoSetFloat AutoSet = AutoSetFloat.None;
    //移動完了時の行動
    public MoveEndAction EndAction = MoveEndAction.None;

    //確定
    public float GetFloat;
    //現在移動時間
    public float TimeSecond = 0.0f;

    //移動を開始
    public void MoveStart(float _StartFloat, float _EndFloat, float _MoveTime, MoveEndAction _EndAction = MoveEndAction.None)
    {
        StartFloat = _StartFloat;
        EndFloat = _EndFloat;
        MoveTime = _MoveTime;
        EndAction = _EndAction;

        TimeSecond = 0.0f;
        ActiveFlag = true;
    }

    //移動を開始（移動開始地点は自身の数値）
    public void MoveStart(float _EndFloat, float _MoveTime, MoveEndAction _EndAction = MoveEndAction.None)
    {
        //適用
        switch (AutoSet)
        {
            case AutoSetFloat.Position_X:
                this.MoveStart(transform.localPosition.x, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Position_Y:
                this.MoveStart(transform.localPosition.y, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Position_Z:
                this.MoveStart(transform.localPosition.z, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Rotation_X:
                this.MoveStart(transform.localRotation.eulerAngles.x, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Rotation_Y:
                this.MoveStart(transform.localRotation.eulerAngles.y, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Rotation_Z:
                this.MoveStart(transform.localRotation.eulerAngles.z, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Scale_X:
                this.MoveStart(transform.localScale.x, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Scale_Y:
                this.MoveStart(transform.localScale.y, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Scale_Z:
                this.MoveStart(transform.localScale.z, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Scale:
                this.MoveStart((transform.localScale.x + transform.localScale.y + transform.localScale.z) / 3.0f, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Image_R:
                this.MoveStart(GetComponent<Image>().color.r, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Image_G:
                this.MoveStart(GetComponent<Image>().color.g, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Image_B:
                this.MoveStart(GetComponent<Image>().color.b, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Image_A:
                this.MoveStart(GetComponent<Image>().color.a, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Sprite_R:
                this.MoveStart(GetComponent<SpriteRenderer>().color.r, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Sprite_G:
                this.MoveStart(GetComponent<SpriteRenderer>().color.g, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Sprite_B:
                this.MoveStart(GetComponent<SpriteRenderer>().color.b, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Sprite_A:
                this.MoveStart(GetComponent<SpriteRenderer>().color.a, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Canvas_R:
                this.MoveStart(GetComponent<CanvasRenderer>().GetColor().r, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Canvas_G:
                this.MoveStart(GetComponent<CanvasRenderer>().GetColor().g, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Canvas_B:
                this.MoveStart(GetComponent<CanvasRenderer>().GetColor().b, _EndFloat, _MoveTime, _EndAction);
                break;

            case AutoSetFloat.Color_Canvas_A:
                this.MoveStart(GetComponent<CanvasRenderer>().GetColor().a, _EndFloat, _MoveTime, _EndAction);
                break;
        }
    }

    //曲線移動を設定
    public void SetCurve(AnimationCurve _Curve)
    {
        Curve = _Curve;
    }

    //曲線移動を初期値に戻す
    public void SetCurve()
    {
        Curve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
    }

    public float get()
    {
        //曲線結果を取得
        float tempCurve = Curve.Evaluate(TimeSecond / MoveTime);

        //逆走中であり、移動完了していれば
        if (isBack && TimeSecond <= 0.0f)
        {
            //初期化
            tempCurve = 0.0f;
        }
        //通常移動であり、移動完了していれば
        else if (MoveTime <= TimeSecond)
        {
            //完了状態にする
            tempCurve = 1.0f;
        }

        //結果を返す
        return StartFloat + ((EndFloat - StartFloat) * tempCurve);
    }

    //更新
    void Update()
    {
        //曲線結果を取得
        float tempCurve = 0.0f;
        if (MoveTime == 0.0f) tempCurve = ActiveFlag ? 1.0f : 0.0f;
        else tempCurve = Curve.Evaluate(TimeSecond / MoveTime);

        //動作が有効であれば
        if (ActiveFlag)
        {
            //----------------------------------------------------------------------------------------------------//

            //逆走中であれば
            if (isBack)
            {
                //時間を減らす
                TimeSecond -= Time.deltaTime;

                //0秒以下なら
                if (TimeSecond <= 0.0f)
                {
                    //初期化
                    TimeSecond = 0.0f;
                    tempCurve = 0.0f;

                    //移動完了後の動作
                    switch (EndAction)
                    {
                        case MoveEndAction.None:
                            ActiveFlag = false;
                            break;

                        case MoveEndAction.Stop_Reset:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Reset;
                        case MoveEndAction.Loop_Reset:
                            TimeSecond = MoveTime;
                            break;

                        case MoveEndAction.Stop_Back:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Back;
                        case MoveEndAction.Loop_Back:
                            isBack = false;
                            break;

                        case MoveEndAction.Stop_FlipBack:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_FlipBack;
                        case MoveEndAction.Loop_FlipBack:
                            float tempFloat = StartFloat;
                            StartFloat = EndFloat;
                            EndFloat = tempFloat;
                            TimeSecond = MoveTime;
                            return;

                        case MoveEndAction.Destroy_Component:
                            Destroy(GetComponent<AutoMoveFloat>());
                            break;

                        case MoveEndAction.Destroy_GameObject:
                            Destroy(gameObject);
                            break;
                    }
                }
            }

            //----------------------------------------------------------------------------------------------------//

            //通常移動であれば
            else
            {
                //時間を加算
                TimeSecond += Time.deltaTime;

                //目標時間に到達したら
                if (MoveTime <= TimeSecond)
                {
                    //完了状態にする
                    TimeSecond = MoveTime;
                    tempCurve = 1.0f;

                    //移動完了後の動作
                    switch (EndAction)
                    {
                        case MoveEndAction.None:
                            ActiveFlag = false;
                            break;

                        case MoveEndAction.Stop_Reset:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Reset;
                        case MoveEndAction.Loop_Reset:
                            TimeSecond = 0.0f;
                            break;

                        case MoveEndAction.Stop_Back:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Back;
                        case MoveEndAction.Loop_Back:
                            isBack = true;
                            break;

                        case MoveEndAction.Stop_FlipBack:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_FlipBack;
                        case MoveEndAction.Loop_FlipBack:
                            float tempFloat = StartFloat;
                            StartFloat = EndFloat;
                            EndFloat = tempFloat;
                            TimeSecond = 0.0f;
                            return;

                        case MoveEndAction.Destroy_Component:
                            Destroy(GetComponent<AutoMoveFloat>());
                            break;

                        case MoveEndAction.Destroy_GameObject:
                            Destroy(gameObject);
                            break;
                    }
                }
            }
        }

        //結果を代入
        GetFloat = StartFloat + ((EndFloat - StartFloat) * tempCurve);

        //適用
        switch (AutoSet)
        {
            case AutoSetFloat.Position_X:
                transform.localPosition = new Vector3(GetFloat, transform.localPosition.y, transform.localPosition.z);
                break;

            case AutoSetFloat.Position_Y:
                transform.localPosition = new Vector3(transform.localPosition.x, GetFloat, transform.localPosition.z);
                break;

            case AutoSetFloat.Position_Z:
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, GetFloat);
                break;

            case AutoSetFloat.Rotation_X:
                transform.localRotation = Quaternion.Euler(GetFloat, transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z);
                break;

            case AutoSetFloat.Rotation_Y:
                transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles.x, GetFloat, transform.localRotation.eulerAngles.z);
                break;

            case AutoSetFloat.Rotation_Z:
                transform.localRotation = Quaternion.Euler(transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, GetFloat);
                break;

            case AutoSetFloat.Scale_X:
                transform.localScale = new Vector3(GetFloat, transform.localScale.y, transform.localScale.z);
                break;

            case AutoSetFloat.Scale_Y:
                transform.localScale = new Vector3(transform.localScale.x, GetFloat, transform.localScale.z);
                break;

            case AutoSetFloat.Scale_Z:
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, GetFloat);
                break;

            case AutoSetFloat.Scale:
                transform.localScale = new Vector3(GetFloat, GetFloat, GetFloat);
                break;

            case AutoSetFloat.Color_Image_R:
                GetComponent<Image>().color = new Color(GetFloat, GetComponent<Image>().color.g, GetComponent<Image>().color.b, GetComponent<Image>().color.a);
                break;

            case AutoSetFloat.Color_Image_G:
                GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetFloat, GetComponent<Image>().color.b, GetComponent<Image>().color.a);
                break;

            case AutoSetFloat.Color_Image_B:
                GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetFloat, GetComponent<Image>().color.a);
                break;

            case AutoSetFloat.Color_Image_A:
                GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, GetFloat);
                break;

            case AutoSetFloat.Color_Sprite_R:
                GetComponent<SpriteRenderer>().color = new Color(GetFloat, GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, GetComponent<SpriteRenderer>().color.a);
                break;

            case AutoSetFloat.Color_Sprite_G:
                GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r, GetFloat, GetComponent<SpriteRenderer>().color.b, GetComponent<SpriteRenderer>().color.a);
                break;

            case AutoSetFloat.Color_Sprite_B:
                GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r, GetComponent<SpriteRenderer>().color.g, GetFloat, GetComponent<SpriteRenderer>().color.a);
                break;

            case AutoSetFloat.Color_Sprite_A:
                GetComponent<SpriteRenderer>().color = new Color(GetComponent<SpriteRenderer>().color.r, GetComponent<SpriteRenderer>().color.g, GetComponent<SpriteRenderer>().color.b, GetFloat);
                break;

            case AutoSetFloat.Color_Canvas_R:
                GetComponent<CanvasRenderer>().SetColor(new Color(GetFloat, GetComponent<CanvasRenderer>().GetColor().g, GetComponent<CanvasRenderer>().GetColor().b, GetComponent<CanvasRenderer>().GetColor().a));
                break;

            case AutoSetFloat.Color_Canvas_G:
                GetComponent<CanvasRenderer>().SetColor(new Color(GetComponent<CanvasRenderer>().GetColor().r, GetFloat, GetComponent<CanvasRenderer>().GetColor().b, GetComponent<CanvasRenderer>().GetColor().a));
                break;

            case AutoSetFloat.Color_Canvas_B:
                GetComponent<CanvasRenderer>().SetColor(new Color(GetComponent<CanvasRenderer>().GetColor().r, GetComponent<CanvasRenderer>().GetColor().g, GetFloat, GetComponent<CanvasRenderer>().GetColor().a));
                break;

            case AutoSetFloat.Color_Canvas_A:
                GetComponent<CanvasRenderer>().SetColor(new Color(GetComponent<CanvasRenderer>().GetColor().r, GetComponent<CanvasRenderer>().GetColor().g, GetComponent<CanvasRenderer>().GetColor().b, GetFloat));
                break;
        }
    }
}