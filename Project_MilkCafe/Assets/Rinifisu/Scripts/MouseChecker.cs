﻿using UnityEngine;
using System.Collections;

public class MouseChecker : MonoBehaviour
{
    // 対象のオブジェクト
    public GameObject MeshObject;

    // マウスが触れている状態
    public bool isTouth = false;

    // マウスがクリックされた状態
    public bool isClick = false;

    // マウスがホールドクリックされた状態
    public bool isHold = false;

    void Update()
    {
        // レイを取得して判定
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        // 自身にマウスが触れていたら
        if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == MeshObject)
        {
            isTouth = true;
            isClick = Input.GetMouseButton(0) && !isHold;
            isHold = Input.GetMouseButton(0);
        }
        else
        {
            isTouth = false;
            isClick = false;
            isHold = false;
        }
    }
}
