﻿using UnityEngine;
using System.Collections;

public class AspectCamara : MonoBehaviour
{
    // Gameビューのサイズ
    [SerializeField]
    private Vector2 gameView;
    public Vector2 GameView
    {
        get
        {
            return gameView;
        }
    }

    // Cameraビューのサイズ
    [SerializeField]
    private Vector2 cameraView;
    public Vector2 CameraView
    {
        get
        {
            return cameraView;
        }
    }

    // 画面中央からのCameraの中心座標
    [SerializeField]
    private Vector2 cameraViewPosition;
    public Vector2 CameraViewPosition
    {
        get
        {
            return cameraViewPosition;
        }
    }

    // Camera
    private Camera myCamera;

    // アスペクト比
    private float aspectRate;
    
    // Start
    void Start()
    {
        this.myCamera = this.GetComponent<Camera>();
        this.aspectRate = (float)this.cameraView.x / this.cameraView.y;
        this.updateScreenRate();

        Screen.SetResolution((int)this.cameraView.x, (int)this.cameraView.y, true);
    }

    // Update
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape)) Application.Quit();

        if (this.isChangeAspect()) return;

        updateScreenRate();
    }

    // スクリーンの比率を更新する。
    private void updateScreenRate()
    {
        // スクリーンに対するゲームビューの幅の比率
        float widthRatio = Screen.width / gameView.x;

        // スクリーンに対するゲームビューの高さの比率
        float heightRatio = Screen.height / gameView.y;

        // 小さい方の比率で調整する。
        float ratio = (widthRatio >= heightRatio) ? heightRatio : widthRatio;

        // カメラのサイズを取得する。(0〜1)
        float camerawWidth = cameraView.x * ratio / Screen.width;
        float camerawHeight = cameraView.y * ratio / Screen.height;

        // スクリーンの中心座標
        float screenCenterX = (1 - camerawWidth) * 0.5f;
        float screenCenterY = (1 - camerawHeight) * 0.5f;

        // カメラの中心座標を比率に合わせて取得する。
        float cameraCenterX = cameraViewPosition.x * ratio / Screen.width;
        float cameraCenterY = cameraViewPosition.y * ratio / Screen.height;

        // カメラのRectを設定する。
        this.myCamera.rect = new Rect(screenCenterX + cameraCenterX,
                                      screenCenterY + cameraCenterY,
                                          camerawWidth,
                                          camerawHeight);
    }

    // アスペクト比が変更されているか
    private bool isChangeAspect()
    {
        return Mathf.Approximately(this.myCamera.aspect, aspectRate);
    }
}