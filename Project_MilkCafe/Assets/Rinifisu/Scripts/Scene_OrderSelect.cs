﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

enum SceneState_OrderSelect
{
    ShowTable,
    ZoomIn,
    ShowBack,

    Wait_Order,

    MenuRoll_In_OrderSelect,
    MenuRoll_In_Front,
    Wait_GameStart,
    MenuRoll_Out_Front,
    MenuRoll_Out_OrderSelect,

    MainGame_WhiteOut,
    MainGame_SceneChange,

    Menu_HideBack_and_ZoomOut,
    Menu_HideTable,
    Menu_SceneChange,
}

public class Scene_OrderSelect : MonoBehaviour
{
    public bool isActive;

    public GameObject Canvas_Front;
    public GameObject Canvas_Back;

    public AutoMoveFloat Table;
    public AutoMoveFloat Menu;

    public List<MouseChecker> Button_Order;
    public GameObject Button_Back;

    public GameObject ZoomIn_Target;
    public GameObject ZoomOut_Target;

    public AutoMoveVector MainCamera;

    public MouseChecker Back_Button_GameStart;
    public MouseChecker Back_Button_Back;

    public WhiteScreen WhiteOut;

    private int SelectValue;

    private AsyncOperation Operation;
    private MapLoader Loader;

    private SceneState_OrderSelect State;
    private SceneState_OrderSelect State_Old;

    private bool State_Once;

    void Awake()
    {
        isActive = false;

        State = SceneState_OrderSelect.ShowTable;
        State_Once = true;
    }

    void SceneChange(SceneState_OrderSelect state)
    {
        State = state;
        State_Once = true;
    }

    void Update()
    {
        if (!isActive) return;

        State_Old = State;

        switch (State)
        {
            case SceneState_OrderSelect.ShowTable:
                {
                    if (State_Once)
                    {
                        Table.ActiveFlag = true;
                        Menu.ActiveFlag = true;
                    }

                    if (!Table.ActiveFlag && !Menu.ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.ZoomIn);
                    }
                }
                break;

            case SceneState_OrderSelect.ZoomIn:
                {
                    if (State_Once)
                    {
                        MainCamera.EndVector = ZoomIn_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.ShowBack);
                    }
                }
                break;

            case SceneState_OrderSelect.ShowBack:
                {
                    if (State_Once)
                    {
                        Button_Back.GetComponent<AutoMoveFloat>().ActiveFlag = true;
                    }

                    if (!Button_Back.GetComponent<AutoMoveFloat>().ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.Wait_Order);
                    }
                }
                break;

            case SceneState_OrderSelect.Wait_Order:
                {
                    if (Button_Back.GetComponent<MouseChecker>().isClick)
                    {
                        SceneChange(SceneState_OrderSelect.Menu_HideBack_and_ZoomOut);
                    }
                    else
                    {
                        for (int i = 0; i < Button_Order.Count; ++i)
                        {
                            if (Button_Order[i] == null || Button_Order[i].GetComponent<MouseChecker>() == null) continue;

                            if (Button_Order[i].GetComponent<MouseChecker>().isClick)
                            {
                                SelectValue = i;
                                SceneChange(SceneState_OrderSelect.MenuRoll_In_OrderSelect);
                            }
                        }
                    }
                }
                break;

            case SceneState_OrderSelect.MenuRoll_In_OrderSelect:
                {
                    if (State_Once)
                    {
                        Canvas_Front.GetComponent<AutoMoveFloat>().ActiveFlag = true;
                    }

                    if (!Canvas_Front.GetComponent<AutoMoveFloat>().ActiveFlag)
                    {
                        Canvas_Front.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
                        SceneChange(SceneState_OrderSelect.MenuRoll_In_Front);
                    }
                }
                break;

            case SceneState_OrderSelect.MenuRoll_In_Front:
                {
                    if (State_Once)
                    {
                        Canvas_Back.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                        Canvas_Back.GetComponent<AutoMoveFloat>().ActiveFlag = true;
                    }

                    if (!Canvas_Back.GetComponent<AutoMoveFloat>().ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.Wait_GameStart);
                    }
                }
                break;

            case SceneState_OrderSelect.Wait_GameStart:
                {
                    if (Back_Button_GameStart.isClick)
                    {
                        SceneChange(SceneState_OrderSelect.MainGame_WhiteOut);
                    }
                    else if (Back_Button_Back.isClick)
                    {
                        SceneChange(SceneState_OrderSelect.MenuRoll_Out_Front);
                    }
                }
                break;

            case SceneState_OrderSelect.MenuRoll_Out_Front:
                {
                    if (State_Once)
                    {
                        Canvas_Back.GetComponent<AutoMoveFloat>().ActiveFlag = true;
                    }

                    if (!Canvas_Back.GetComponent<AutoMoveFloat>().ActiveFlag)
                    {
                        Canvas_Back.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
                        SceneChange(SceneState_OrderSelect.MenuRoll_Out_OrderSelect);
                    }
                }
                break;

            case SceneState_OrderSelect.MenuRoll_Out_OrderSelect:
                {
                    if (State_Once)
                    {
                        Canvas_Front.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                        Canvas_Front.GetComponent<AutoMoveFloat>().ActiveFlag = true;
                    }

                    if (!Canvas_Front.GetComponent<AutoMoveFloat>().ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.Wait_Order);
                    }
                }
                break;

            case SceneState_OrderSelect.MainGame_WhiteOut:
                {
                    if (State_Once)
                    {
                        WhiteOut.isActive = true;
                    }

                    if (!WhiteOut.isActive)
                    {
                        SceneChange(SceneState_OrderSelect.MainGame_SceneChange);
                    }
                }
                break;

            case SceneState_OrderSelect.MainGame_SceneChange:
                {
                    if (State_Once)
                    {
                        //仮
                        SceneBridge.StageValue = SelectValue;

                        // 非同期にシーンを読み込み始める
                        Operation = SceneManager.LoadSceneAsync("GamePlayScene");
                        Operation.allowSceneActivation = false;

                        Loader = new MapLoader();
                        Loader.load("MapData/Stage" + SceneBridge.StageValue);
                    }

                    // 次のシーンに移行
                    if (!Operation.allowSceneActivation && 0.9f <= Operation.progress)
                    {
                        Operation.allowSceneActivation = true;
                        SceneBridge.mapData = Loader.getMapData();
                        SceneBridge.mapHeight = SceneBridge.mapData.GetLength(0);
                        SceneBridge.mapWidth = SceneBridge.mapData.GetLength(1);
                    }
                }
                break;

            case SceneState_OrderSelect.Menu_HideBack_and_ZoomOut:
                {
                    if (State_Once)
                    {
                        Button_Back.GetComponent<AutoMoveFloat>().ActiveFlag = true;

                        MainCamera.EndVector = ZoomOut_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!Button_Back.GetComponent<AutoMoveFloat>().ActiveFlag && !MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.Menu_HideTable);
                    }
                }
                break;

            case SceneState_OrderSelect.Menu_HideTable:
                {
                    if (State_Once)
                    {
                        Table.ActiveFlag = true;
                        Menu.ActiveFlag = true;
                    }

                    if (!Table.ActiveFlag && !Menu.ActiveFlag)
                    {
                        SceneChange(SceneState_OrderSelect.Menu_SceneChange);
                    }
                }
                break;

            case SceneState_OrderSelect.Menu_SceneChange:
                {
                    if (State_Once)
                    {
                        GetComponent<Scene_Menu>().isActive = true;
                        isActive = false;

                        SceneChange(SceneState_OrderSelect.ShowTable);
                    }
                }
                break;
        }

        if (State_Old == State) State_Once = false;
    }
}