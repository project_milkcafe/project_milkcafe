﻿using UnityEngine;
using System.Collections;

public class ClickEffect : MonoBehaviour
{
    public GameObject Effect;
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject CreateEffect = Instantiate(Effect);

            CreateEffect.transform.SetParent(transform, false);
            CreateEffect.transform.position = Input.mousePosition;
        }
    }
}
