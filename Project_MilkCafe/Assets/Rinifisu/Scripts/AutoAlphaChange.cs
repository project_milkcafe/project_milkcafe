﻿using UnityEngine;
using System.Collections;

public class AutoAlphaChange : MonoBehaviour
{
    public float TimeSecond = 0.0f;
    public float Speed = 5.0f;

    public float Center = 0.75f;
    public float Width = 0.25f;
    
    void Update()
    {
        TimeSecond += Time.deltaTime;

        GetComponent<CanvasRenderer>().SetColor(new Color(1.0f, 1.0f, 1.0f, Center + Mathf.Sin(TimeSecond * Speed) * Width));
    }
}
