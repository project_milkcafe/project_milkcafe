﻿using UnityEngine;

//移動完了後の動作
public enum MoveEndAction
{
    None,                   //何もしない
    Loop_Reset,             //ループ：初期化
    Loop_Back,              //ループ：逆走
    Loop_FlipBack,          //ループ：開始座標と終了座標の反転（曲線移動の反転回避ができる）
    Stop_Reset,             //ループ準備を行い停止：初期化
    Stop_Back,              //ループ準備を行い停止：逆走
    Stop_FlipBack,          //ループ準備を行い停止：開始座標と終了座標の反転（曲線移動の反転回避ができる）
    Destroy_Component,      //コンポーネントを消去
    Destroy_GameObject,     //オブジェクトを消去
}

//自動設定する項目内容
public enum AutoSetVector
{
    None,
    Position,
    Rotation,
    Scale,
}

//ベクトルの移動をサポートするクラス
public class AutoMoveVector : MonoBehaviour
{
    //動作フラグ
    public bool ActiveFlag = false;
    //動作方面
    public bool isBack = false;

    //移動開始ベクトル
    public Vector3 StartVector;
    //移動終了ベクトル
    public Vector3 EndVector;

    //移動時間
    public float MoveTime = 0.0f;

    //曲線移動（上下無制限、左右 0.0～1.0 まで）
    public AnimationCurve Curve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    //自動設定する項目内容
    public AutoSetVector AutoSet = AutoSetVector.None;
    //移動完了時の行動
    public MoveEndAction EndAction = MoveEndAction.None;

    //確定ベクトル
    public Vector3 GetVector;
    //現在移動時間
    public float TimeSecond = 0.0f;

    //移動を開始
    public void MoveStart(Vector3 _StartVector, Vector3 _EndVector, float _MoveTime, MoveEndAction _EndAction = MoveEndAction.None)
    {
        StartVector = _StartVector;
        EndVector = _EndVector;
        MoveTime = _MoveTime;
        EndAction = _EndAction;

        TimeSecond = 0.0f;
        ActiveFlag = true;
    }

    //移動を開始（移動開始地点は自身のベクトル）
    public void MoveStart(Vector3 _EndVector, float _MoveTime, MoveEndAction _EndAction = MoveEndAction.None)
    {
        //適用
        switch (AutoSet)
        {
            //座標を設定
            case AutoSetVector.Position:
                this.MoveStart(transform.localPosition, _EndVector, _MoveTime, _EndAction);
                break;

            //回転軸を設定
            case AutoSetVector.Rotation:
                this.MoveStart(transform.localRotation.eulerAngles, _EndVector, _MoveTime, _EndAction);
                break;

            //大きさを設定
            case AutoSetVector.Scale:
                this.MoveStart(transform.localScale, _EndVector, _MoveTime, _EndAction);
                break;
        }
    }

    //曲線移動を設定
    public void SetCurve(AnimationCurve _Curve)
    {
        Curve = _Curve;
    }

    //曲線移動を初期値に戻す
    public void SetCurve()
    {
        Curve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
    }

    public Vector3 get()
    {
        //曲線結果を取得
        float tempCurve = MoveTime != 0.0f ? Curve.Evaluate(TimeSecond / MoveTime) : 1.0f;

        //逆走中であり、移動完了していれば
        if (isBack && TimeSecond <= 0.0f)
        {
            //初期化
            tempCurve = 0.0f;
        }
        //通常移動であり、移動完了していれば
        else if (MoveTime <= TimeSecond)
        {
            //完了状態にする
            tempCurve = 1.0f;
        }

        //結果を返す
        return StartVector + ((EndVector - StartVector) * tempCurve);
    }

    //更新
    void Update()
    {
        //曲線結果を取得
        float tempCurve = 0.0f;
        if (MoveTime == 0.0f) tempCurve = ActiveFlag ? 1.0f : 0.0f;
        else tempCurve = Curve.Evaluate(TimeSecond / MoveTime);

        //動作が有効であれば
        if (ActiveFlag)
        {
            //----------------------------------------------------------------------------------------------------//

            //逆走中であれば
            if (isBack)
            {
                //時間を減らす
                TimeSecond -= Time.deltaTime;

                //0秒以下なら
                if (TimeSecond <= 0.0f)
                {
                    //初期化
                    TimeSecond = 0.0f;
                    tempCurve = 0.0f;

                    //移動完了後の動作
                    switch (EndAction)
                    {
                        case MoveEndAction.None:
                            ActiveFlag = false;
                            break;

                        case MoveEndAction.Stop_Reset:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Reset;
                        case MoveEndAction.Loop_Reset:
                            TimeSecond = MoveTime;
                            break;

                        case MoveEndAction.Stop_Back:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Back;
                        case MoveEndAction.Loop_Back:
                            isBack = false;
                            break;

                        case MoveEndAction.Stop_FlipBack:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_FlipBack;
                        case MoveEndAction.Loop_FlipBack:
                            Vector3 tempVector = StartVector;
                            StartVector = EndVector;
                            EndVector = tempVector;
                            TimeSecond = MoveTime;
                            return;

                        case MoveEndAction.Destroy_Component:
                            Destroy(GetComponent<AutoMoveVector>());
                            break;

                        case MoveEndAction.Destroy_GameObject:
                            Destroy(gameObject);
                            break;
                    }
                }
            }

            //----------------------------------------------------------------------------------------------------//

            //通常移動であれば
            else
            {
                //時間を加算
                TimeSecond += Time.deltaTime;

                //目標時間に到達したら
                if (MoveTime <= TimeSecond)
                {
                    //完了状態にする
                    TimeSecond = MoveTime;
                    tempCurve = 1.0f;

                    //移動完了後の動作
                    switch (EndAction)
                    {
                        case MoveEndAction.None:
                            ActiveFlag = false;
                            break;

                        case MoveEndAction.Stop_Reset:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Reset;
                        case MoveEndAction.Loop_Reset:
                            TimeSecond = 0.0f;
                            break;

                        case MoveEndAction.Stop_Back:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_Back;
                        case MoveEndAction.Loop_Back:
                            isBack = true;
                            break;

                        case MoveEndAction.Stop_FlipBack:
                            ActiveFlag = false;
                            goto case MoveEndAction.Loop_FlipBack;
                        case MoveEndAction.Loop_FlipBack:
                            Vector3 tempVector = StartVector;
                            StartVector = EndVector;
                            EndVector = tempVector;
                            TimeSecond = 0.0f;
                            return;

                        case MoveEndAction.Destroy_Component:
                            Destroy(GetComponent<AutoMoveVector>());
                            break;

                        case MoveEndAction.Destroy_GameObject:
                            Destroy(gameObject);
                            break;
                    }
                }
            }
        }

        //結果を代入
        GetVector = StartVector + ((EndVector - StartVector) * tempCurve);

        //適用
        switch (AutoSet)
        {
            //座標を設定
            case AutoSetVector.Position:
                transform.localPosition = GetVector;
                break;

            //回転軸を設定
            case AutoSetVector.Rotation:
                transform.localRotation = Quaternion.Euler(GetVector);
                break;

            //大きさを設定
            case AutoSetVector.Scale:
                transform.localScale = GetVector;
                break;
        }
    }
}