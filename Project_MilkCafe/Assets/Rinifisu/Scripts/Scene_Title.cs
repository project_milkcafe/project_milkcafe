﻿using UnityEngine;
using System.Collections;

enum SceneState_Title
{
    TouchWait,
    Move_TouchToStart,
    OpenDoor_and_ZoomIn,
    SceneChange,

    CloseDoor_and_ZoomOut,
}

public class Scene_Title : MonoBehaviour
{
    public bool isActive;

    public WhiteScreen Screen;
    public AutoMoveVector TouchToStart;

    public AutoMoveVector Door_Left;
    public AutoMoveVector Door_Right;

    public AutoMoveVector MainCamera;
    public GameObject MoveCamera_Target;

    private SceneState_Title State;
    private SceneState_Title State_Old;

    private bool State_Once;

    void Awake()
    {
        State = SceneState_Title.TouchWait;
        State_Once = true;
    }

    void SceneChange(SceneState_Title state)
    {
        State = state;
        State_Once = true;
    }

    void Update()
    {
        if (!isActive) return;

        State_Old = State;

        switch (State)
        {
            case SceneState_Title.TouchWait:
                {
                    if (Input.GetMouseButton(0))
                    {
                        SceneChange(SceneState_Title.Move_TouchToStart);
                    }
                }
                break;

            case SceneState_Title.Move_TouchToStart:
                {
                    if (State_Once)
                    {
                        TouchToStart.ActiveFlag = true;
                    }

                    if (!TouchToStart.ActiveFlag)
                    {
                        SceneChange(SceneState_Title.OpenDoor_and_ZoomIn);
                    }
                }
                break;

            case SceneState_Title.OpenDoor_and_ZoomIn:
                {
                    if (State_Once)
                    {
                        Door_Left.ActiveFlag = true;
                        Door_Right.ActiveFlag = true;
                        
                        MainCamera.EndVector = MoveCamera_Target.transform.position;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_Title.SceneChange);
                    }
                }
                break;

            case SceneState_Title.SceneChange:
                {
                    if (State_Once)
                    {
                        GetComponent<Scene_Menu>().isActive = true;
                        isActive = false;

                        SceneChange(SceneState_Title.CloseDoor_and_ZoomOut);
                    }
                }
                break;

            case SceneState_Title.CloseDoor_and_ZoomOut:
                {
                    if (State_Once)
                    {
                        TouchToStart.ActiveFlag = true;

                        Door_Left.ActiveFlag = true;
                        Door_Right.ActiveFlag = true;

                        MainCamera.EndVector = Vector3.zero;
                        MainCamera.ActiveFlag = true;
                    }

                    if (!MainCamera.ActiveFlag)
                    {
                        SceneChange(SceneState_Title.TouchWait);
                    }
                }
                break;
        }

        if (State_Old == State) State_Once = false;
    }
}
